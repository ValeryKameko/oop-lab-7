﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Diagnostics;

namespace oop_lab7
{
    class Program
    {
        private class RoleStatistics
        {
            public Dictionary<string, int> Roles { get; private set; }

            public RoleStatistics()
            {
                Roles = new Dictionary<string, int>();
            }

            public void AddRole(string role)
            {
                if (Roles.ContainsKey(role))
                    Roles[role]++;
                else
                    Roles[role] = 1;
            }

            public void Write()
            {
                var tableWriter = new TableConsoleWriter();
                tableWriter.Write(Roles.Select(entry => new List<String>(){ entry.Key, entry.Value.ToString() }).ToList());
            }
        }

        private static RoleStatistics GatherStatistics(UserXmlReader reader)
        {
            var statistics = new RoleStatistics();
            int header = 100;
            while (reader.ReadUser())
            {
                var user = reader.User;
                var userRole = user.Role ?? "no_role";
                statistics.AddRole(userRole);
            }
            return statistics;
        }

        static void Main(string[] args)
        {
            bool needFileRead = true;

            while (needFileRead)
            {
                needFileRead = false;

                Console.WriteLine("Enter path to file: ");
                var pathString = Console.ReadLine();

                if (pathString == "")
                    break;

                RoleStatistics statistics;
                try
                {
                    Stopwatch stopWatch = new Stopwatch();

                    using (var fs = new FileStream(pathString, FileMode.Open, FileAccess.Read))
                    using (var reader = XmlReader.Create(fs))
                    {
                        var userReader = new UserXmlReader(reader);
                        stopWatch.Start();
                        statistics = GatherStatistics(userReader);
                        stopWatch.Stop();

                    }
                    statistics.Write();
                    Console.WriteLine($"Elapsed Time = {stopWatch.Elapsed}");
                }
                catch (FileNotFoundException e)
                {
                    Console.WriteLine(String.Format($"File \"{e.FileName}\" not found."));
                    needFileRead = true;
                    continue;
                }
            }
        }
    }
}
