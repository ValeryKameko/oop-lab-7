﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_lab7
{
    class User
    {
        public String Role { get; private set; }

        public String UID { get; private set; }
        public String Name { get; private set; }
        public String Email { get; private set; }
        public String City { get; private set; }
        public String Address { get; private set; }
        public String Phone { get; private set; }
        public Gender Gender { get; private set; }
        public List<String> Skills { get; private set; }

        protected User()
        {
            Skills = new List<String>();
        }

        public class UserBuilder
        {
            User user;

            public UserBuilder()
            {
                user = new User()
                {
                    Gender = Gender.Unknown
                };
            }

            public UserBuilder(User user)
            {
                this.user = user;
            }

            public UserBuilder SetUID(String uid)
            {
                user.UID = uid;
                return this;
            }

            public UserBuilder SetName(String name)
            {
                user.Name = name;
                return this;
            }

            public UserBuilder SetEmail(String email)
            {
                user.Email = email;
                return this;
            }

            public UserBuilder SetCity(String city)
            {
                user.City = city;
                return this;
            }

            public UserBuilder SetAddress(String address)
            {
                user.Address = address;
                return this;
            }

            public UserBuilder SetPhone(String phone)
            {
                user.Phone = phone;
                return this;
            }

            public UserBuilder SetGender(Gender gender)
            {
                user.Gender = gender;
                return this;
            }

            public UserBuilder SetRole(String role)
            {
                user.Role = role;
                return this;
            }

            public UserBuilder AddSkill(String skill)
            {
                user.Skills.Add(skill);
                return this;
            }

            public User Build()
            {
                return user;
            }
        }
    }

    public enum Gender
    {
        Unknown, Male, Female
    }
}

