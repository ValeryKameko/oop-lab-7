﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace oop_lab7
{
    using UserFIeldXmlReaderHandler = Func<User.UserBuilder, XmlReader, bool>;

    class UserXmlReader
    {
        public User User { get; private set; }

        private XmlReader reader;
        private Dictionary<String, UserFIeldXmlReaderHandler> handlers;

        public UserXmlReader(XmlReader reader)
        {
            this.reader = reader;
            handlers = new Dictionary<string, UserFIeldXmlReaderHandler>()
            {
                { "uid", ReadUID },
                { "name", ReadName },
                { "email", ReadEmail },
                { "city", ReadCity },
                { "address", ReadAddress },
                { "phone", ReadPhone },
                { "gender", ReadGender },
                { "skills", ReadSkills },
            };
        }

        public bool ReadUser()
        {
            if (!reader.ReadToFollowing("user"))
                return false;
            User = ReadUser(reader);
            return true;
        }

        private User ReadUser(XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "user")
                return null;
            var builder = new User.UserBuilder();
            var role = reader.GetAttribute("role");
            if (role != null)
                builder.SetRole(role);

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "user")
                    break;
                if (reader.NodeType != XmlNodeType.Element || reader.IsEmptyElement)
                    continue;

                if (handlers.TryGetValue(reader.Name, out UserFIeldXmlReaderHandler handler))
                {
                    handler(builder, reader);
                }
            }

            return builder.Build();
        }

        private bool ReadUID(User.UserBuilder userBuilder, XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "uid")
                return false;
            var uid = ReadInnerText(reader);
            userBuilder.SetUID(uid);
            return true;
        }

        private bool ReadName(User.UserBuilder userBuilder, XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "name")
                return false;
            var name = ReadInnerText(reader);
            userBuilder.SetName(name);
            return true;
        }

        private bool ReadEmail(User.UserBuilder userBuilder, XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "email")
                return false;
            var email = ReadInnerText(reader);
            userBuilder.SetEmail(email);
            return true;
        }

        private bool ReadCity(User.UserBuilder userBuilder, XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "city")
                return false;
            var city = ReadInnerText(reader);
            userBuilder.SetCity(city);
            return true;
        }

        private bool ReadAddress(User.UserBuilder userBuilder, XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "address")
                return false;
            var address = ReadInnerText(reader);
            userBuilder.SetAddress(address);
            return true;
        }

        private bool ReadPhone(User.UserBuilder userBuilder, XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "phone")
                return false;
            var phone = ReadInnerText(reader);
            userBuilder.SetPhone(phone);
            return true;
        }

        private bool ReadGender(User.UserBuilder userBuilder, XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "gender")
                return false;
            var genderString = ReadInnerText(reader);
            Gender gender;
            switch (genderString.ToString())
            {
                case "male":
                    gender = Gender.Male;
                    break;
                case "female":
                    gender = Gender.Female;
                    break;
                default:
                    return false;
            }
            userBuilder.SetGender(gender);
            return true;
        }

        private bool ReadSkills(User.UserBuilder userBuilder, XmlReader reader)
        {
            if (!reader.IsStartElement() || reader.Name != "skills")
                return false;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "skills")
                    break;
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "skill")
                {
                    var skill = ReadInnerText(reader);
                    userBuilder.AddSkill(skill);
                }
            }
            return true;
        }

        private String ReadInnerText(XmlReader reader)
        {
            return reader.ReadElementContentAsString();
        }
    }
}
