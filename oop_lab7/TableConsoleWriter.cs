﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_lab7
{
    class TableConsoleWriter
    {
        public TableConsoleWriter()
        {
            
        }

        public void Write(List<List<String>> table)
        {
            var columnCount = table.Aggregate(0, (accumulator, row) => Math.Max(accumulator, row.Count));
            var columnWidths = Enumerable.Repeat(0, columnCount).ToList();
            foreach (var row in table)
            {
                for (int i = 0; i < row.Count; i++)
                {
                    columnWidths[i] = Math.Max(columnWidths[i], row[i].Length + 4);
                }
            }

            Console.WriteLine(CreateDelimiterRow(columnWidths));
            foreach (var row in table)
            {
                var fullRow = row.Concat(Enumerable.Repeat("", columnCount - row.Count));
                var values = columnWidths.Zip(fullRow, (width, element) => new Tuple<int, string>(width, element)).ToList();
                Console.WriteLine(CreateEmptyRow(columnWidths));
                Console.WriteLine(CreateValueRow(values));
                Console.WriteLine(CreateEmptyRow(columnWidths));
                Console.WriteLine(CreateDelimiterRow(columnWidths));
            }
        }

        private String CreateDelimiterRow(List<int> columnWidths)
        {
            var pieces = columnWidths.Select(width => String.Concat(Enumerable.Repeat("-", width)));
            return "+" + String.Join("+", pieces) + "+";
        }

        private String CreateEmptyRow(List<int> columnWidths)
        {
            var pieces = columnWidths.Select(width => String.Concat(Enumerable.Repeat(" ", width)));
            return "|" + String.Join("|", pieces) + "|";
        }


        public string PadBoth(string source, int length)
        {
            int spaces = length - source.Length;
            int padLeft = spaces / 2 + source.Length;
            return source.PadLeft(padLeft).PadRight(length);

        }

        private String CreateValueRow(List<Tuple<int, String>> row)
        {
            var pieces = row.Select(entry => PadBoth(entry.Item2, entry.Item1));
            return "|" + String.Join("|", pieces) + "|";
        }
    }
}
